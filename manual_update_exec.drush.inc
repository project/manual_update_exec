<?php

/**
 * @file
 * Drush integration for the manual_update_exec module.
 */

/**
 * Implements hook_drush_command().
 */
function manual_update_exec_drush_command() {
  $items['execute-update'] = array(
    'description' => dt('Execute update manually'),
    'aliases' => array('exup'),
    'options' => array(
      'module' => 'The name of module to call its update',
      'number' => 'The number of update or "all" to execute all updates.',
      'change-schema-version' => 'Change module schema version to update number',
    ),
    'examples' => array(
      'exup --module=rules --number=7214' => 'Manual (re)execution of rules_update_7214().',
    ),
  );

  return $items;
}

/**
 * A command callback.
 */
function drush_manual_update_exec_execute_update() {
  if (drush_get_context('DRUSH_SIMULATE')) {
    drush_log(dt('updatedb command does not support --simulate option.'), 'ok');
    return TRUE;
  }

  if (!$module = drush_get_option('module', NULL)) {
    $module = drush_prompt(dt('Please, enter a MODULE of update you want to run'));
  }
  if (!$number = drush_get_option('number', NULL)) {
    $number = drush_prompt(dt('Please, enter a NUMBER of update you want to run or "all" to update all unfinished updates'));
  }

  if (!module_exists($module)) {
    return drush_set_error(dt('Module "@module" not found', array('@module' => $module)));
  }

  // Prepare drupal for update execution.
  drush_include_engine('drupal', 'update', drush_drupal_major_version());
  update_main_prepare();

  if ($number == MANUAL_UPDATE_EXEC_ALL_UPDATES) {
    list($pending, $start) = updatedb_status();
    $number = $start[$module];
    $until_last= TRUE;
  }
  else {
    $until_last = FALSE;
  }

  $schema_versions = drupal_get_schema_versions($module);
  if (array_search($number, $schema_versions) === FALSE) {
    return drush_set_error(dt('Update "@update" not found in module "@module"', array('@update' => $number, '@module' => $module)));
  }

  if ($change_schema_version = (bool) drush_get_option('change-schema-version', FALSE)) {
    drush_print(dt('Schema version WILL BE changed to @number. You can use --change-schema-version option to manage this.', array('@number' => $number)));
  }
  else {
    drush_print(dt('Schema version WILL NOT BE changed. You can use --change-schema-version option to manage this.'));
  }

  $last_version = end($schema_versions);
  $difference = $last_version - $number;
  if ($difference > 0) {
    $params = array(
      '@module' => $module,
      '@last' => $last_version,
      '@chosen' => $number,
      '@diff' => $difference,
    );
    if ($until_last) {
      drush_print(dt('Will be processed all updates from @chosen to @last.', $params));
    }
    else {
      drush_print(dt('Current "@module" version is @last. You chose @chosen (versions difference is @diff).', $params));
    }
  }

  if (!drush_confirm(dt('Do you confirm update(s) execution?'))) {
    return drush_user_abort();
  }

  // Called forked update function.
  manual_update_exec_call_update(array($module => $number), $change_schema_version, $until_last);
}

/**
 * Fork of @see drush_update_batch().
 */
function manual_update_exec_call_update($start, $change_schema_version = FALSE, $until_last = FALSE) {
  $updates = update_resolve_dependencies($start);

  $dependency_map = array();
  list($function, $update) = each($updates);
  $dependency_map[$function] = !empty($update['reverse_paths']) ? array_keys($update['reverse_paths']) : array();

  $operations = array();
  foreach ($updates as $update) {
    if ($update['allowed']) {
      if ($change_schema_version && isset($start[$update['module']])) {
        drupal_set_installed_schema_version($update['module'], $update['number'] - 1);
        unset($start[$update['module']]);
      }
      $function = $update['module'] . '_update_' . $update['number'];
      $operations[] = array('manual_update_exec_do_one', array($update['module'], $update['number'], $dependency_map[$function], $change_schema_version));
    }
    // To call only chosen update (will be first).
    if (!$until_last) {
      break;
    }
  }

  if (!$operations) {
    return drush_set_error('No allowed updates found');
  }

  $batch['operations'] = $operations;
  $batch += array(
    'title' => 'Updating',
    'init_message' => 'Starting updates',
    'error_message' => 'An unrecoverable error has occurred. You can find the error message below. It is advised to copy it to the clipboard for reference.',
    'finished' => 'drush_update_finished',
    'file' => 'includes/update.inc',
  );
  batch_set($batch);
  drush_backend_batch_process('updatedb-batch-process');
}

/**
 * Fork of @see drush_update_do_one().
 */
function manual_update_exec_do_one($module, $number, $dependency_map, $change_schema_version, &$context) {
  $function = $module . '_update_' . $number;

  // If this update was aborted in a previous step, or has a dependency that
  // was aborted in a previous step, go no further.
  if (!empty($context['results']['#abort']) && array_intersect($context['results']['#abort'], array_merge($dependency_map, array($function)))) {
    return;
  }

  $context['log'] = FALSE;

  $ret = array();
  if (function_exists($function)) {
    try {
      if ($context['log']) {
        Database::startLog($function);
      }

      drush_log("Executing " . $function);
      $ret['results']['query'] = $function($context['sandbox']);
      $ret['results']['success'] = TRUE;
    }
    catch (Exception $e) {
      $ret['#abort'] = array('success' => FALSE, 'query' => $e->getMessage());
      drush_set_error('DRUPAL_EXCEPTION', $e->getMessage());
    }

    if ($context['log']) {
      $ret['queries'] = Database::getLog($function);
    }
  }

  if (isset($context['sandbox']['#finished'])) {
    $context['finished'] = $context['sandbox']['#finished'];
    unset($context['sandbox']['#finished']);
  }

  if (!isset($context['results'][$module])) {
    $context['results'][$module] = array();
  }
  if (!isset($context['results'][$module][$number])) {
    $context['results'][$module][$number] = array();
  }
  $context['results'][$module][$number] = array_merge($context['results'][$module][$number], $ret);

  if (!empty($ret['#abort'])) {
    // Record this function in the list of updates that were aborted.
    $context['results']['#abort'][] = $function;
  }

  // Record the schema update if it was completed successfully.
  if ($context['finished'] == 1 && empty($ret['#abort']) && $change_schema_version) {
    drupal_set_installed_schema_version($module, $number);
  }

  $context['message'] = 'Performed update: ' . $function;
}
