--------------------------------------------------------------------------------
  manual_update_exec module Readme
  http://drupal.org/project/manual_update_exec
--------------------------------------------------------------------------------

Contents:
=========
1. ABOUT
2. INSTALLATION
3. USAGE EXAMPLES
4. CREDITS

1. ABOUT
========

This module provides developers a possibility to execute updates manually,
without manipulations with schema version (but they're also possible).
Update calls support sandboxes and other "updb" features.

2. INSTALLATION
===============

Install as usual, see http://drupal.org/node/895232 for further information.

3. USAGE EXAMPLES
===============

1. Already performed contrib update reexecution

    drush exup --module=rules --number=7214

2. Performing updates for single module

    drush exup --module=devel --number=all --change-schema-version

4. CREDITS
==========

Project page: http://drupal.org/project/manual_update_exec

- Drupal 7 -

Authors:
* Yudkin Evgeny - https://www.drupal.org/u/evgeny_yudkin
